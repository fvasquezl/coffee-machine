class Coffee:
    def __init__(self):
        self.__machine_status = True
        self.__coffee_selected = ''
        self.__cost_coffee = 0
        self.__mount_deposited = 0
        self.__machine_actions = ("report", "off")
        self.__machine_resources = {
            "water": {"total": 800, "sign": "ml"},
            "milk": {"total": 500, "sign": "ml"},
            "coffee": {"total": 500, "sign": "g"},
            "money": {"total": 300, "sign": "$"}
        }
        self.__coffee_options = {
            "expresso": {"water": 230, "milk": 120, "coffee": 20, "money": 3.00},
            "latte": {"water": 200, "milk": 150, "coffee": 24, "money": 2.50},
            "cappuccino": {"water": 190, "milk": 160, "coffee": 30, "money": 2.25},
        }
        self.__coins = {'quarters': 0.25, 'dimes': 0.10, 'nickles': 0.05, 'pennies': 0.01}

    def process_resources(self):
        change = float
        if self.mount_deposited > self.cost_coffee:
            self.__machine_resources['money']['total'] += self.mount_deposited
            change = round(self.mount_deposited - self.cost_coffee , 2)
            print(f"Your Change ${change:.2f}")

        for key in self.machine_resources.keys():
            if key == 'money':
                self.machine_resources[key]['total'] -= change
            else:
                self.machine_resources[key]['total'] -= self.coffee_options[self.coffee_selected][key]

    def check_transaction(self):
        self.cost_coffee = self.coffee_options[self.coffee_selected]['money']
        print(f"Total Coins ${self.mount_deposited:.2f}")
        if self.mount_deposited < self.cost_coffee:
            print("Sorry that's not enough money. Money refunded.")
            print(f"return ${self.mount_deposited}")
            return False
        else:
            print(f"Here is your {self.coffee_selected}. Enjoy!.")
            return True

    def process_coins(self):
        print("Insert Coins")
        for key in self.coins.keys():
            while True:
                try:
                    qty = int(input(f"{key.capitalize()}: "))
                    break
                except ValueError:
                    print(f'"{key.capitalize()}", Must be int')
            self.mount_deposited += qty * self.coins[key]

        self.mount_deposited = round(self.mount_deposited, 2)

    def check_resources(self):
        for key in self.machine_resources.keys():
            if self.machine_resources[key]['total'] <= self.coffee_options[self.coffee_selected][key]:
                print(f'Sorry there is not enough {key}.')
                return False
        return True

    def report(self):
        for key in self.__machine_resources.keys():
            if key == 'money':
                print(
                    f"{key.capitalize()}: {self.machine_resources[key]['sign']}{self.machine_resources[key]['total']:.2f}")
            else:
                print(
                    f"{key.capitalize()}: {self.machine_resources[key]['total']}{self.machine_resources[key]['sign']}")

    def menu(self):
        self.coffee_selected = input(f"What would you like? ({'/'.join(self.__coffee_options.keys()).lower()}):")
        try:
            if self.coffee_selected in self.coffee_options.keys() or self.coffee_selected in self.__machine_actions:
                return
            else:
                print('Please check your option')
        except ValueError:
            print('Something wrong with your answer ')
        return self.menu()

    def play(self):
        while self.machine_status:
            self.menu()
            if self.coffee_selected == 'off':
                self.machine_status = False
            elif self.coffee_selected == 'report':
                self.report()
            else:
                if self.check_resources():
                    self.process_coins()
                    if self.check_transaction():
                        self.process_resources()
            self.mount_deposited = 0
            self.cost_coffee = 0

    @property
    def coffee_selected(self):
        return self.__coffee_selected

    @coffee_selected.setter
    def coffee_selected(self, value):
        self.__coffee_selected = value

    @property
    def mount_deposited(self):
        return self.__mount_deposited

    @mount_deposited.setter
    def mount_deposited(self, value):
        self.__mount_deposited = value

    @property
    def cost_coffee(self):
        return self.__cost_coffee

    @cost_coffee.setter
    def cost_coffee(self, value):
        self.__cost_coffee = value

    @property
    def machine_status(self):
        return self.__machine_status

    @machine_status.setter
    def machine_status(self, value):
        self.__machine_status = value

    @property
    def machine_resources(self):
        return self.__machine_resources

    @machine_resources.setter
    def machine_resources(self, value):
        self.__machine_resources = value

    @property
    def coins(self):
        return self.__coins

    @property
    def coffee_options(self):
        return self.__coffee_options


c = Coffee()
c.play()



